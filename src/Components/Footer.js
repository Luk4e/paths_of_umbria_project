import React from 'react';

const Footer = () => {

  const styleFooter = {
    height: '50px',
  };

  return(
    <footer style={styleFooter}>
      <p>Page&apos;s footer</p>
    </footer>
  );
};

export default Footer;
